//The File containing the app constants

class AppConstants {
  static const signUp = "Sign Up";
  static const signIn = "Sign In";
  static const email = "Email";
  static const phoneNumber = "Phone Number";
  static const password = "Password";
  static const confirmPassword = "Confirm Password";
  static const name = "Name";
  static const agreeToTermsAndPolicies = "By creating an account, you agree to our Terms and Polcies.";
  static const rememberMe = "Remember Me";
  static const donotHaveAccount = "Don't have an account? ";
  static const signUpHere = "Sign Up Here!";
  static const verifyOTP = "Verify OTP";
  static const enterOtpHeading = "Please enter the one time password that we have sent to your associated phone number.";
  static const verify = "Verify";
  static const forgotPassword = "Forgot Password";
  static const enterRegisteredEmail = "Please enter your registered email address.";
  static const didForgetPassword = "Forgot Password?";
  static const submit = "Submit";
  static const resetPassword = "Reset Password";
  static const newPassword = "New Password";
  static const ok = "OK";
  static const cancel = "Cancel";
  static const maxPasswordLength = 12;
  static const maxPhoneNumberLength = 10;
}

class ValidationMessage {
  static const enterEmail = "Enter email";
  static const enterPassword = "Enter password";
  static const enterValidEmail = "Enter a valid email address";
  static const enterValidPasswordLength = "Enter a password between 6 to 12 characters";
  static const signInSuccessFull = "Thanks for signing in!!";
  static const passwordsDoNotMatch = "Your passwords donot match.";
  static const enterValidOTP = "Please enter a valid 4 digit one time password that we have sent to your phone number.";
  static const passwordReset = "Your password has been reset successfully.";
  static const enterValidPhoneNumber = "Enter a valid phone number";
}

class Regex {
  static const email = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9-]+\\.[A-Za-z]{2,6}";
}

enum ScreenFrom {
  forgotPassword, verifyRegistrationOtp
}