import 'package:flutter/material.dart';
import 'Constants.dart';
import 'RoundedButton.dart';
import 'VerifyOTPScreen.dart';
import 'Utility.dart';
import 'Validation.dart';

class ForgotPasswordScreen extends StatelessWidget {

  String email;
  final _formKey = GlobalKey<FormState>();
  FocusNode _emailFocusNode = FocusNode();

  //Custom helpers
  _backPressed(BuildContext context) {
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: new AppBar(
        leading: new IconButton(
            icon: new Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            onPressed: () {
              _backPressed(context);
            }),
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0.0,
        title: new Text(
          AppConstants.forgotPassword,
          style: Theme.of(context).textTheme.title,
        ),
      ),
      body: new Container(
        padding: const EdgeInsets.only(top: 30.0, left: 20.0, right: 20.0),
        child: new Column(
          children: <Widget>[
            new Expanded(
              child: new Container(
                child: new Column(
                  children: <Widget>[
                    new Text(AppConstants.enterRegisteredEmail, style: new TextStyle(fontSize: 16.0),),
                    new Padding(padding: const EdgeInsets.only(top: 10.0)),
                    new Form(
                      key: _formKey,
                      child: new TextFormField(
                        focusNode: _emailFocusNode,
                        keyboardType: TextInputType.emailAddress,
                        decoration: new InputDecoration(
                          border: new OutlineInputBorder(),
                        ),
                        style: new TextStyle(
                          fontSize: 16.0,
                          color: Colors.grey
                        ),
                        onFieldSubmitted: (value) {
                          _emailFocusNode.unfocus();
                        },
                        validator: (value) {
                          print("validation called");
                          //return Validation.validateEmail(value);
                        },
                      ),
                    )
                  ],
                ),
              ),
            ),
            new Container(
              padding: const EdgeInsets.only(bottom: 40.0),
              child: new MyRoundedButton(AppConstants.submit, titleColor: Colors.white, backgroundColor: Colors.lightBlue, actionTapped: () {
                  _submitTapped(context);
              },),
            )
          ],
        ),
      ),
    );
  }

  _submitTapped(BuildContext context) {
    //if (_formKey.currentState.validate()) {
      print("submit tapped");
      Navigator.push(context, MaterialPageRoute(builder: (context) => new VerifyOTPScreen(ScreenFrom.forgotPassword)));
    //}
  }
}
