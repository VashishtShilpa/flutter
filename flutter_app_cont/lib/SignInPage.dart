import 'package:flutter/material.dart';
import 'Constants.dart';
import 'RoundedButton.dart';
import 'SignUpPage.dart';
import 'ForgotPassword.dart';
import 'routes.dart';
import 'Utility.dart';
import 'Validation.dart';

class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  //Properties
  final _logoSize = 100.0;
  FocusNode emailFocusNode;
  FocusNode passwordFocusNode;
  final _formKey = GlobalKey<FormState>();

  bool rememberMe = false;

  //Custom helpers
  _backPressed() {
    Navigator.pop(context);
  }

  _signInTapped() {
    print("Sign in tapped");
  }

  rememberMeTapped(bool value) {
    setState(() {
      rememberMe = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        leading: new IconButton(
            icon: new Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            onPressed: _backPressed),
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0.0,
        title: new Text(
          AppConstants.signIn,
          //style: new TextStyle(color: Colors.white),
          style: Theme.of(context).textTheme.title,
        ),
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: new SingleChildScrollView(
        child: new Container(
          color: Theme.of(context).primaryColor,
          padding: new EdgeInsets.only(left: 20.0, right: 20.0, top: 90.0),
          child: new Column(
            children: <Widget>[
              new Stack(
                overflow: Overflow.visible,
                alignment: Alignment.center,
                children: <Widget>[
                  new Container(
                    height: 230.0,
                    decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius:
                            new BorderRadius.all(Radius.circular(10.0))),
                    padding:
                        EdgeInsets.only(top: 55.0, left: 15.0, right: 15.0),
                    child: signInForm(),
                  ),
                  _appLogo(),
                ],
              ),
              new Row(
                children: <Widget>[
                  new Checkbox(
                    value: rememberMe,
                    onChanged: rememberMeTapped,
                    activeColor: Colors.blueGrey,
                  ),
                  new Expanded(
                    child: new Text(
                      AppConstants.rememberMe,
                      style: new TextStyle(color: Colors.white, fontSize: 16.0),
                    ),
                  )
                ],
              ),
              new Padding(padding: const EdgeInsets.only(top: 20.0)),
              new MyRoundedButton(
                AppConstants.signIn,
                actionTapped: () {
                  if (_formKey.currentState.validate()) {
                    Utility.showAlert(
                        context, ValidationMessage.signInSuccessFull, AppConstants.ok,
                        () {
                      Navigator.pop(context);
                    });
                  }
                },
              ),
              new Padding(padding: const EdgeInsets.only(top: 20.0)),
              new InkWell(
                child: new Text(
                  AppConstants.didForgetPassword,
                  style: new TextStyle(decoration: TextDecoration.underline),
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => new ForgotPasswordScreen()));
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  //return the circular logo
  Widget _appLogo() {
    return new Positioned(
      child: new Container(
        width: _logoSize,
        height: _logoSize,
        decoration: new BoxDecoration(
            borderRadius: new BorderRadius.circular(_logoSize / 2),
            color: Colors.blue,
            image: new DecorationImage(
                image: new AssetImage("assets/images/appLogo.jpeg"),
                fit: BoxFit.fitHeight)),
      ),
      top: -(_logoSize / 2),
    );
  }

  Widget signInForm() {
    return new Form(
        key: _formKey,
        child: new Column(
          children: <Widget>[
            new TextFormField(
              decoration: new InputDecoration(
                  prefixIcon: new Icon(Icons.email),
                  hintText: AppConstants.email),
              focusNode: emailFocusNode,
              onFieldSubmitted: (value) {
                FocusScope.of(context).requestFocus(passwordFocusNode);
              },
              validator: (value) {
                return Validation.validateEmail(value);
              },
            ),
            new Padding(padding: const EdgeInsets.only(top: 20.0)),
            new TextFormField(
              decoration: new InputDecoration(
                  prefixIcon: new Icon(Icons.lock),
                  hintText: AppConstants.password),
              focusNode: passwordFocusNode,
              obscureText: true,
              maxLength: AppConstants.maxPasswordLength,
              onFieldSubmitted: (value) {
                passwordFocusNode.unfocus();
              },
              validator: (value) {
                return Validation.validatePassword(value);
              },
            ),
          ],
        ));
  }
}
