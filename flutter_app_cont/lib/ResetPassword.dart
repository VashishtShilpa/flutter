import 'package:flutter/material.dart';
import 'Constants.dart';
import 'RoundedButton.dart';
import 'routes.dart';
import 'Validation.dart';
import 'Utility.dart';

class ResetPasswordView extends StatefulWidget {
  @override
  _ResetPasswordViewState createState() => _ResetPasswordViewState();
}

class _ResetPasswordViewState extends State<ResetPasswordView> {
  //Properties
  FocusNode newPasswordFocusNode = new FocusNode();
  FocusNode confirmPasswordFocusNode = new FocusNode();
  final _passwordController = TextEditingController();
  final _confirmPasswordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: new AppBar(
        leading: new IconButton(
            icon: new Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0.0,
        title: new Text(
          AppConstants.resetPassword,
          //style: new TextStyle(color: Colors.white),
          style: Theme.of(context).textTheme.title,
        ),
      ),
      body: new Container(
        padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
        child: new Column(
          children: <Widget>[
            new Expanded(
              child: new SingleChildScrollView(
                child: new Form(
                    key: _formKey,
                    child: new Column(
                      children: <Widget>[
                        new TextFormField(
                          decoration: new InputDecoration(
                              prefixIcon: new Icon(Icons.lock),
                              hintText: AppConstants.newPassword),
                          focusNode: newPasswordFocusNode,
                          obscureText: true,
                          controller: _passwordController,
                          validator: (value) {
                            return Validation.validatePassword(value);
                          },
                          onFieldSubmitted: (value) {},
                        ),
                        new Padding(padding: const EdgeInsets.only(top: 20.0)),
                        new TextFormField(
                          decoration: new InputDecoration(
                              prefixIcon: new Icon(Icons.lock),
                              hintText: AppConstants.confirmPassword),
                          focusNode: confirmPasswordFocusNode,
                          obscureText: true,
                          controller: _confirmPasswordController,
                          onFieldSubmitted: (value) {},
                          validator: (value) {
                            return Validation.validatePasswordsForEquality(
                                _passwordController.text,
                                _confirmPasswordController.text);
                          },
                        ),
                      ],
                    )),
              ),
            ),
            new Container(
              padding: const EdgeInsets.only(top: 10.0, bottom: 40.0),
              child: new MyRoundedButton(
                AppConstants.submit,
                titleColor: Colors.white,
                backgroundColor: Colors.lightBlue,
                actionTapped: () {
                  //Navigator.popUntil(context, predicate)
                  if (_formKey.currentState.validate()) {
                    Utility.showAlert(
                        context, ValidationMessage.passwordReset, AppConstants.ok,
                            () {
                          Navigator.popUntil(
                              context, ModalRoute.withName(Routes.toSignIn));
                        });
                  }

                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
