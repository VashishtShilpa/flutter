import 'package:flutter/material.dart';
import 'Constants.dart';
import 'RoundedButton.dart';
import 'SignUpPage.dart';
import 'SignInPage.dart';
import 'routes.dart';

//This will contain the initial screen containing the actions for sign up and sign in
class SignUpSignInScreen extends StatelessWidget {

  //Properties
  final logoSize = 150.0;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
      ),
      body: new Container(
        color: Colors.white,
        child: new Container(
          alignment: Alignment.center,
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Container(
                width: logoSize,
                height: logoSize,
                decoration: new BoxDecoration(
                    borderRadius: new BorderRadius.circular(logoSize / 2),
//                    color: Colors.blue,
                    image: new DecorationImage(
                        image: new AssetImage("assets/images/appLogo.jpeg"), fit: BoxFit.fitHeight)),
              ),
              new Padding(padding: const EdgeInsets.only(top: 60.0)),
              new MyRoundedButton(AppConstants.signIn, titleColor: Colors.white, backgroundColor: Colors.lightBlue, actionTapped: () {
                this._signInPressed(context);
              },),
              new Padding(padding: const EdgeInsets.only(top: 20.0)),
              new MyRoundedButton(AppConstants.signUp, titleColor: Colors.white, backgroundColor: Colors.lightBlue, actionTapped: () {
                this._signUpPressed(context);
              },),
            ],
          ),
        ),
      ),
    );
  }

  _signInPressed(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => SignInPage(), settings: new RouteSettings(name: Routes.toSignIn)));
  }
  
  _signUpPressed(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => SignUpPage()));
  }
}
