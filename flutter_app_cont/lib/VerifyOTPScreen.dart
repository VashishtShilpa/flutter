import 'package:flutter/material.dart';
import 'Constants.dart';
import 'RoundedButton.dart';
import 'ResetPassword.dart';
import 'Utility.dart';

class VerifyOTPScreen extends StatefulWidget {

  ScreenFrom screenFrom;

  //Initializer
  VerifyOTPScreen(this.screenFrom);

  @override
  _VerifyOTPScreenState createState() => _VerifyOTPScreenState();
}

class _VerifyOTPScreenState extends State<VerifyOTPScreen> {
  //Properties
  final fieldHeight = 30.0;
  FocusNode firstOtpFocusNode = new FocusNode();
  FocusNode secondOtpFocusNode = new FocusNode();
  FocusNode thirdOtpFocusNode = new FocusNode();
  FocusNode fourthOtpFocusNode = new FocusNode();
  final TextEditingController _firstOtpFieldController = new TextEditingController();
  final TextEditingController _secondOtpFieldController = new TextEditingController();
  final TextEditingController _thirdOtpFieldController = new TextEditingController();
  final TextEditingController _fourthOtpFieldController = new TextEditingController();
  final totalCount = 4;
  final maxLength = 1;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          AppConstants.verifyOTP,
          style: Theme.of(context).textTheme.title,
        ),
        backgroundColor: Theme.of(context).primaryColor,
        leading: new IconButton(
            icon: new Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
        elevation: 0.0,
      ),
      resizeToAvoidBottomPadding: false,
      body: new Container(
        padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 35.0),
        child: new Column(
          children: <Widget>[

            new Expanded(
              child: new Container(
                child: new Column(
                  children: <Widget>[
                    new Text(AppConstants.enterOtpHeading, textAlign: TextAlign.center, style: new TextStyle(color: Theme.of(context).primaryColor, fontSize: 16.0, fontWeight: FontWeight.bold),),

                    new Padding(padding: const EdgeInsets.only(bottom: 20.0)),

                    _otpFields(),
                  ],
                ),
              ),
            ),

            new Container(
              width: MediaQuery.of(context).size.width,
              //height: 50.0,
              padding: const EdgeInsets.only(bottom: 40.0),
              child: new MyRoundedButton(AppConstants.verify, titleColor: Colors.white, backgroundColor: Colors.lightBlue, actionTapped: () {
                print("otp entered in ${_enteredOtp()}");
                _submitOtp(_enteredOtp());
              },),
            )
          ],
        ),
      ),
    );
  }

  //return the otp fields in a row with equal spacing
  Widget _otpFields() {
    return new Container(
      padding: const EdgeInsets.only(left: 20.0, right: 20.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          new Container(
            width: (MediaQuery.of(context).size.width - 110) / totalCount,
            height: fieldHeight,
            child: new TextField(
              focusNode: firstOtpFocusNode,
              textAlign: TextAlign.center,
              controller: _firstOtpFieldController,
              keyboardType: TextInputType.number,
              decoration: new InputDecoration(
                  border: OutlineInputBorder()),
              onChanged: (newValue) {
                if (newValue.length == maxLength) {
                    FocusScope.of(context).requestFocus(secondOtpFocusNode);
                }
              },
            ),
          ),

          new Container(
            width: (MediaQuery.of(context).size.width - 110) / totalCount,
            height: fieldHeight,
            child: new TextField(
              focusNode: secondOtpFocusNode,
              textAlign: TextAlign.center,
              controller: _secondOtpFieldController,
              keyboardType: TextInputType.number,
              decoration: new InputDecoration(
                  border: OutlineInputBorder()),
              onChanged: (newValue) {
                if (newValue.length == maxLength) {
                  FocusScope.of(context).requestFocus(thirdOtpFocusNode);
                } else if (newValue.length < maxLength) {
                  FocusScope.of(context).requestFocus(firstOtpFocusNode);
                }
              },
            ),
          ),

          new Container(
            width: (MediaQuery.of(context).size.width - 110) / totalCount,
            height: fieldHeight,
            child: new TextField(
              focusNode: thirdOtpFocusNode,
              textAlign: TextAlign.center,
              keyboardType: TextInputType.number,
              controller: _thirdOtpFieldController,
              decoration: new InputDecoration(
                  border: OutlineInputBorder()),
              onChanged: (newValue) {
                if (newValue.length == maxLength) {
                  FocusScope.of(context).requestFocus(fourthOtpFocusNode);
                } else if (newValue.length < maxLength) {
                  FocusScope.of(context).requestFocus(secondOtpFocusNode);
                }
              },
            ),
          ),

          new Container(
            width: (MediaQuery.of(context).size.width - 110) / totalCount,
            height: fieldHeight,
            child: new TextField(
              focusNode: fourthOtpFocusNode,
              textAlign: TextAlign.center,
              controller: _fourthOtpFieldController,
              keyboardType: TextInputType.number,
              decoration: new InputDecoration(
                  border: OutlineInputBorder()),
              onChanged: (newValue) {
                if (newValue.length == maxLength) {
                  fourthOtpFocusNode.unfocus();
                } else if (newValue.length < maxLength) {
                  FocusScope.of(context).requestFocus(thirdOtpFocusNode);
                }
              },
            ),
          )

        ],
      ),
    );
  }

  //get the values from textFields
  String _enteredOtp() {
    final value1 = _firstOtpFieldController.text;
    final value2 = _secondOtpFieldController.text;
    final value3 = _thirdOtpFieldController.text;
    final value4 = _fourthOtpFieldController.text;

    return (value1 + value2 + value3 + value4);
  }

  _submitOtp(String value) {
    if (value.isEmpty) {
      Utility.showAlert(context, AppConstants.enterOtpHeading, AppConstants.ok, () {
        Navigator.pop(context);
      });
      return;
    }
    if (value.length < totalCount) {
      Utility.showAlert(context, ValidationMessage.enterValidOTP, AppConstants.ok, () {
        Navigator.pop(context);
      });
      return;
    }
    switch (widget.screenFrom) {
      case ScreenFrom.forgotPassword:
        //to reset password screen
      Navigator.push(context, MaterialPageRoute(builder: (context) => new ResetPasswordView()));
      break;
      case ScreenFrom.verifyRegistrationOtp:
        //to home screen
        Utility.showAlert(context, ValidationMessage.signInSuccessFull, AppConstants.ok, () {
          Navigator.pop(context);
        });
        break;
    }
  }
}
