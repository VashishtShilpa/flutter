import 'package:flutter/material.dart';
import 'package:flutter_app_cont/Constants.dart';
import 'RoundedButton.dart';
import 'package:flutter_app_cont/VerifyOTPScreen.dart';
import 'Validation.dart';

class SignUpPage extends StatefulWidget {
  @override
  SignUpPageState createState() {
    // TODO: implement createState
    return SignUpPageState();
  }
}

class SignUpPageState extends State<SignUpPage> {
  //Properties
  final textFieldPadding = const EdgeInsets.only(top: 20.0);
  final logoSize = 100.0;
  var hasAgreedToTermsPolicies = false;
  FocusNode nameFocusNode = new FocusNode();
  FocusNode emailFocusNode = new FocusNode();
  FocusNode phoneNumberFocusNode = new FocusNode();
  FocusNode passwordFocusNode = new FocusNode();
  FocusNode confirmPasswordFocusNode = new FocusNode();
  final _formKey = GlobalKey<FormState>();

  TextEditingController _passwordController = new TextEditingController();
  TextEditingController _confirmPasswordController = new TextEditingController();

  //Custom helpers
  checkBoxPressed(bool value) {
    setState(() {
      hasAgreedToTermsPolicies = !hasAgreedToTermsPolicies;
    });
  }

  backPressed() {
    Navigator.pop(context);
  }

  signUpTapped(BuildContext context) {
    if (_formKey.currentState.validate()) {
      Navigator.push(context, MaterialPageRoute(builder: (context) => new VerifyOTPScreen(ScreenFrom.verifyRegistrationOtp)));
    }
  }

  //change focus-> current focus references the current responder and nextFocus references the next responder
  changeFocus(FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          AppConstants.signUp,
          style: Theme.of(context).textTheme.title,
        ),
        elevation: 0.0,
//        leading: new Icon(
//          Icons.arrow_back,
//          color: Colors.white,
//        ),
        leading: new IconButton(
            icon: new Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            onPressed: backPressed),
      ),
      backgroundColor: Theme.of(context).primaryColor,
      resizeToAvoidBottomPadding: false,
      body: SingleChildScrollView(
        child: new Container(
            color: Theme.of(context).primaryColor,
            padding: const EdgeInsets.only(top: 50.0, left: 20.0, right: 20.0),
            child: new Column(
              children: <Widget>[
                new Stack(
                  overflow: Overflow.visible,
                  alignment: Alignment.center,
                  children: <Widget>[
                    new Container(
                      height: 400.0,
                      decoration: new BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                              new BorderRadius.all(Radius.circular(10.0))),
                      padding:
                          EdgeInsets.only(top: 55.0, left: 15.0, right: 15.0),
                      child: form(),
                    ),
                    _appLogo(),
                  ],
                ),
                new Padding(padding: textFieldPadding),
                new Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new Row(
                      children: <Widget>[
                        new Checkbox(
                          value: hasAgreedToTermsPolicies,
                          onChanged: checkBoxPressed,
                          activeColor: Colors.blueGrey,
                        ),
                        new Expanded(
                          child: new Text(
                            AppConstants.agreeToTermsAndPolicies,
                            style: new TextStyle(
                                color: Colors.white, fontSize: 16.0),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
                new Padding(padding: textFieldPadding),
                new MyRoundedButton(
                  AppConstants.signUp,
                  actionTapped: () {
                    signUpTapped(context);
                  },
                ),
              ],
            )),
      ),
    );
  }

  //Form with text form fields
  Widget form() {
    return new Form(
      key: _formKey,
        child: new ListView(
      children: <Widget>[
        //Name Text field
        new TextFormField(
          decoration: new InputDecoration(
              prefixIcon: new Icon(Icons.email), hintText: AppConstants.name),
          focusNode: nameFocusNode,
          onFieldSubmitted: (value) {
            changeFocus(nameFocusNode, emailFocusNode);
          },
          validator: (value) {
            if (value.isEmpty) {
              return "Enter name.";
            }
          },
        ),

        new Padding(padding: textFieldPadding),

        //Email text field
        new TextFormField(
          decoration: new InputDecoration(
              prefixIcon: new Icon(Icons.email), hintText: AppConstants.email),
          focusNode: emailFocusNode,
          keyboardType: TextInputType.emailAddress,
          onFieldSubmitted: (value) {
            changeFocus(emailFocusNode, phoneNumberFocusNode);
          },
          validator: (value) {
            return Validation.validateEmail(value);
          },
        ),

        new Padding(padding: textFieldPadding),

        //Phone number text field
        new TextFormField(
          decoration: new InputDecoration(
              prefixIcon: new Icon(Icons.contact_phone),
              hintText: AppConstants.phoneNumber),
          focusNode: phoneNumberFocusNode,
          maxLength: AppConstants.maxPasswordLength,
          keyboardType: TextInputType.number,
          onFieldSubmitted: (value) {
            changeFocus(phoneNumberFocusNode, passwordFocusNode);
          },
          validator: (value) {
            if (value.length < AppConstants.maxPhoneNumberLength) {
              return ValidationMessage.enterValidPhoneNumber;
            }
          },
        ),

        new Padding(padding: textFieldPadding),

        //Password text field
        new TextFormField(
          decoration: new InputDecoration(
              prefixIcon: new Icon(Icons.lock),
              hintText: AppConstants.password),
          focusNode: passwordFocusNode,
          obscureText: true,
          controller: _passwordController,
          onFieldSubmitted: (value) {
            changeFocus(passwordFocusNode, confirmPasswordFocusNode);
          },
          validator: (value) {
            return Validation.validatePassword(value);
          },
        ),

        new Padding(padding: textFieldPadding),

        //Confirm Password text field
        new TextFormField(
          decoration: new InputDecoration(
              prefixIcon: new Icon(Icons.lock),
              hintText: AppConstants.confirmPassword),
          focusNode: confirmPasswordFocusNode,
          obscureText: true,
          controller: _confirmPasswordController,
          onFieldSubmitted: (value) {
            confirmPasswordFocusNode.unfocus();
          },
          validator: (value) {
            if (value.isEmpty) {
              return ValidationMessage.enterValidPasswordLength;
            }
            return Validation.validatePasswordsForEquality(_passwordController.text, value);
          },
        ),

        //new Padding(padding: textFieldPadding),
      ],
    ));
  }

  //return the circular logo
  Widget _appLogo() {
    return new Positioned(
      child: new Container(
        width: logoSize,
        height: logoSize,
        decoration: new BoxDecoration(
            borderRadius: new BorderRadius.circular(logoSize / 2),
            color: Colors.blue,
            image: new DecorationImage(
                image: new AssetImage("assets/images/appLogo.jpeg"),
                fit: BoxFit.fitHeight)),
      ),
      top: -(logoSize / 2),
    );
  }
}
