import 'package:flutter/material.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => new _SignUpState();
}

class _SignUpState extends State<SignUp> {
  TextEditingController _nameController;

  @override
  void initState() {
    _nameController = new TextEditingController();

    _nameController.addListener((){
      onValueChange(_nameController.text);
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(),
      body: new Column(
        children: <Widget>[
          new TextFormField(
            controller: _nameController,
          decoration: new InputDecoration(
            contentPadding: EdgeInsets.zero,

          ),
          )
        ],
      ),
    );
  }

  void onValueChange(String text) {
    print("Entered text: $text");
  }
}
