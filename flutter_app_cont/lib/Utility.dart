import 'package:flutter/material.dart';

class Utility {
  //show alert dialog
  static showAlert(BuildContext context, String content, String action1,
      VoidCallback action1Handler,
      {String title: "Invalid Input",
      String action2,
      VoidCallback action2Handler}) {
    var alert = new AlertDialog(
      title: new Text(title),
      content: new Text(content),
      actions: Utility.alertActionHandlers(action1, action1Handler),
    );
    showDialog(
        context: context,
        builder: (context) {
          return alert;
        });
  }

  static List<Widget> alertActionHandlers(String action1, VoidCallback action1Handler,
      {String action2, VoidCallback action2Handler}) {
    List<Widget> widgets;
    if (action2 == null) {
      widgets = <Widget>[
        new FlatButton(
            onPressed: action1Handler,
            child: new Text(
              action1,
              style: new TextStyle(fontSize: 18.0),
            )),
      ];
    } else {
      widgets = <Widget>[
        new FlatButton(
            onPressed: action1Handler,
            child: new Text(
              action1,
              style: new TextStyle(fontSize: 18.0),
            )),
        new FlatButton(
            onPressed: action2Handler,
            child: new Text(
              action2,
              style: new TextStyle(fontSize: 18.0),
            )),
      ];
    }
    return widgets;
  }
}
