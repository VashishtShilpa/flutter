import 'Constants.dart';

class Validation {
  static String validateEmail(String value) {
    print("email $value");
    print("is Empty: ${value.isEmpty}");
    if (value.isEmpty) {
      print("in if condition");
      return ValidationMessage.enterEmail;
    }
    RegExp regexExp = new RegExp(Regex.email);
    if (regexExp.hasMatch(value) == false) {
      return ValidationMessage.enterValidEmail;
    }
    return null;
  }

  static String validatePassword(String value) {
    if (value.isEmpty) {
      return ValidationMessage.enterPassword;
    }
    if (value.length < 6 || value.length > 12) {
      return ValidationMessage.enterValidPasswordLength;
    }
    return null;
  }

  static String validatePasswordsForEquality(String password1, String password2) {
    if (password1 != password2) {
      return ValidationMessage.passwordsDoNotMatch;
    }
    return null;
  }
}
