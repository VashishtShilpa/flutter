import 'package:flutter/material.dart';
import 'Constants.dart';

class MyRoundedButton extends StatefulWidget {
  //Properties
  String title;
  var titleColor;
  var backgroundColor;
  Function actionTapped;  //on pressed button handler

  //Initialization
 // MyRoundedButton(this.title, this.titleColor, this.backgroundColor);
  MyRoundedButton(this.title, {this.titleColor: Colors.lightBlue, this.backgroundColor: Colors.white, this.actionTapped});

  @override
  RoundedButtonState createState() {
    // TODO: implement createState
    return RoundedButtonState();
  }
}

class RoundedButtonState extends State<MyRoundedButton> {

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    // TODO: implement build
    return Container(
      width: size.width,
      margin: new EdgeInsets.only(left: 10.0, right: 10.0),
      child: new RaisedButton(
        onPressed: widget.actionTapped,
        child: Container(
          height: 50.0,
          alignment: Alignment.center,
          child: new Text(
            widget.title,
            style: new TextStyle(color: widget.titleColor, fontSize: 18.0),
            textAlign: TextAlign.center,
          ),
          decoration: new BoxDecoration(
              color: widget.backgroundColor,
              borderRadius: new BorderRadius.all(Radius.circular(10.0))),
        ),
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(10.0),
        ),
        color: widget.backgroundColor,
      ),
    );
  }
}
